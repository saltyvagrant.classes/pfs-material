install_exercises:
  pkg.installed:
    - name: git
  git.latest:
    - name: https://gitlab.com/saltyvagrant.classes/pfs-exercises.git
    - target: /home/vagrant/.exercises
    - user: vagrant
    - bare: True
    - require:
      - pkg: install_exercises

install_exercises_to_bashrc:
  file.append:
    - name: /home/vagrant/.bashrc
    - text:
      - 'alias exercise="git --work-tree=${HOME} --git-dir=${HOME}/.exercises checkout "'

set_initial_exercise:
  cmd.run:
    - name: 'git --work-tree=${HOME} --git-dir=${HOME}/.exercises checkout 01.01'
    - runas: vagrant
    - creates:
      - /home/vagrant/bash
      - /home/vagrant/lua
      - /home/vagrant/python
    - require:
      - file: install_exercises_to_bashrc
      - git: install_exercises
