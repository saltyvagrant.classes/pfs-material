common:
  pip:
    - mypy==0.982
    - flake8==5.0.4
    - black==22.8.0
    - pre-commit==2.20.0
    - python-lsp-server[Rope]==1.5.0
    - pyls-flake8==0.4.0
    - pyls-mypy==0.1.8
    - python-lsp-black==1.2.1
    - pylsp-rope==0.1.10
    - pytest-bdd==6.0.1
    - pytest==7.1.3
  packages:
    - python-is-python3
python:
  version: 3.8.2
states:
  - python.pyenv
  - python.poetry
