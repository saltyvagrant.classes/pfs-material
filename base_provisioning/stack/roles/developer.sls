common:
  packages:
    - git
    - vim
    - tmux
    - tree
    - ruby-full
    - build-essential
states:
  - common_packages
