states:
  - docker.software.service
  - concourse
docker:
  wanted:
    - docker
    - compose
  pkg:
    docker:
      use_upstream: repo
