pyenv-deps:
  pkg.installed:
    - pkgs:
      - make
      - build-essential
      - libssl-dev
      - zlib1g-dev
      - libbz2-dev
      - libreadline-dev
      - libsqlite3-dev
      - wget
      - curl
      - llvm
      - git

install_pyenv:
  pyenv.installed:
    - name: {{ salt['pillar.get']('python.version','3.8.2') }}
    - default: True
    - user: vagrant
    - require:
      - pkg: pyenv-deps

install_pyenv_to_profile:
  file.append:
    - name: /home/vagrant/.profile
    - text:
      - 'export PYENV_ROOT="${HOME}/.pyenv"'
      - 'export PATH="${PYENV_ROOT}/bin:${PATH}"'
      - 'command -v pyenv > /dev/null 2>&1 && eval "$(pyenv init -)" ; true'
