include:
  - docker

concourse-install-docker-compose:
  pip.installed:
    - name: docker-compose >= 1.5

concourse-install-compose-file:
  file.managed:
    - name: /opt/concourse/docker-compose.yml
    - source: salt://concourse/files/docker-compose.yml
    - template: jinja
    - makedirs: True

# Pull the images and create the network
# This is a workaround as dockercompose.up does not work properly
# when run via module.run (works perfectly on command line though)

concourse-force-concourse-image:
  docker_image.present:
    - name: concourse/concourse
    - tag: latest
    - require: 
      - service: docker-software-service-running-docker

concourse-force-postgres-image:
  docker_image.present:
    - name: postgres
    - tag: latest
    - require: 
      - service: docker-software-service-running-docker

concourse-force-concourse-network:
  docker_network.present:
    - name: concourse_default
    - require: 
      - service: docker-software-service-running-docker

concourse-start:
  module.run:
    - dockercompose.up:
      - path: /opt/concourse
    - require: 
      - file: concourse-install-compose-file
      - pip: concourse-install-docker-compose
      - docker_network: concourse-force-concourse-network
      - docker_image: concourse-force-concourse-image
      - docker_image: concourse-force-postgres-image
      - service: docker-software-service-running-docker

concourse-install-fly:
  cmd.run:
    - name: 'sleep 10; curl -L "http://localhost:8080/api/v1/cli?arch=amd64&platform=linux" >/usr/local/bin/fly && chmod +x /usr/local/bin/fly || rm -f /usr/local/bin/fly'
    - creates: /usr/local/bin/fly
    - require:
      - module: concourse-start
