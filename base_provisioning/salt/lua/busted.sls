install-lua-busted:
  pkg.installed:
    - name: luarocks
  cmd.run:
    - name: luarocks install busted
    - unless: command -v busted > /dev/null  2>&1
    - require:
      - pkg: install-lua-busted

