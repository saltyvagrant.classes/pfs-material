install-shellspec:
  pkg.installed:
    - name: curl
  cmd.run:
    - name: "curl -fsSL https://git.io/shellspec | sh -s -- -y"
    - cwd: /home/vagrant
    - runas: vagrant
    - creates: /home/vagrant/.local/bin/shellspec
    - require:
      - pkg: install-shellspec
